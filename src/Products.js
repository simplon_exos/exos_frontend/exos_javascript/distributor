export class Products {
    /**
     *
     * @param {string} name 
     * @param {integer} id
     */
    constructor(name, id, source) {
        this.name = name;
        this.id = id;
        this.source = source;
    }
    drawProducts() {
        /*let snackButton = document.createElement('div');
        snackButton.classList.add('snackButton');*/
        let snack = document.createElement('img');
        snack.classList.add('img');
        // 1. Pour afficher des images différentes, il faut mettre la source en paramètre dans le constructeur de Products et utiliser this pour pointer dessus
        snack.src = this.source;
        return snack;
    }

    showProducts() {
        alert(`${this.name} costs ${this.price} and its id is: ${this.id}.`);

    }
    choseProducts() {
            alert(`You chose ${this.name} you have to pay ${this.price}`);
        }
        /*dropProducts() {

        }*/
}