import { Drinks } from "./Drinks";
import { Candies } from "./Candies";
import { Products } from "./Products";

// 3. Penser à ajouter les url sources pour chaque produit dans sa construction
export const allProducts = [
    new Drinks("ginger beer", 1, "./img/gingerbeer.jpg", 7, ),
    new Drinks("vodka lemon", 2, "./img/vodkalemon.jpg", 10),
    new Drinks("charitea", 3, "./img/charitea.jpg", 5),
    new Drinks("chouffe", 4, "./img/chouffe.png", 7),
    new Drinks("ice americano", 5, "./img/iceamericano.jpg", 4),
    new Drinks("green tea", 6, "./img/greentea.jpg", 5),
    new Drinks("trappist", 7, "./img/trappiste.png", 7),
    new Drinks("cuvee des trolls", 8, "./img/cuveedestrolls.jpg", 8),
    new Drinks("milk", 9, "./img/milk.jpg", 3),
    new Candies("bubblies", 10, "./img/bubblizz.png", 3),
    new Candies("eggs", 11, "./img/eggs.jpg", 5),
    new Candies("langue de chat", 12, "./img/languedechat.png", 6),
    new Candies("Arlequin", 13, "./img/arlequin.png", 10000),
    new Candies("Dragibus", 14, "./img/dragibus.jpg", 7),
    new Candies("Marshmallow", 15, "./img/chamallow.png", 3)
];
let container = document.querySelector('#container');

for (const product of allProducts) {
    // On applique la méthode draw pour chaque produit
    // et on l'appendchild dans le container
    // On le stocke dans une variable pour pouvoir mettre 
    // un eventlistener dessus
    let item = container.appendChild(product.drawProducts());
    item.addEventListener('click', function() {
        product.showProducts();
    });
};

// au click sur le boutton submit, on lance la méthode choseProducts sur l'index du produit concerné
let submitBtn = document.querySelector('#submit');
let choiceInput = document.querySelector('#choice');
submitBtn.addEventListener('click', function() {
    let choice = choiceInput.value;
    allProducts[parseInt(choice) - 1].choseProducts();
});

// Et maintenant faut afficher l'image voulue dans le réceptacle en bas ;)