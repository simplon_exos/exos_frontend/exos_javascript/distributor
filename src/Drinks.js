import { Products } from "./Products";

export class Drinks extends Products {
    /**
     * 
     * @param {integer} price 
     */
    constructor(name, id, source, price) {
        super(name, id, source);
        this.price = price;
    }
    /*createDrinks() {

    }*/
}