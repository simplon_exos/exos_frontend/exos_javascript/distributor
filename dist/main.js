/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Candies.js":
/*!************************!*\
  !*** ./src/Candies.js ***!
  \************************/
/*! exports provided: Candies */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Candies", function() { return Candies; });
/* harmony import */ var _Products__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Products */ "./src/Products.js");


class Candies extends _Products__WEBPACK_IMPORTED_MODULE_0__["Products"] {
    /**
     * 
     * @param {integer} price 
     */
    constructor(name, id, source, price) {
        super(name, id,source);
        this.price = price;
    }
    /*createCandy() {

    }*/
}

/***/ }),

/***/ "./src/Drinks.js":
/*!***********************!*\
  !*** ./src/Drinks.js ***!
  \***********************/
/*! exports provided: Drinks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Drinks", function() { return Drinks; });
/* harmony import */ var _Products__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Products */ "./src/Products.js");


class Drinks extends _Products__WEBPACK_IMPORTED_MODULE_0__["Products"] {
    /**
     * 
     * @param {integer} price 
     */
    constructor(name, id, source, price) {
        super(name, id, source);
        this.price = price;
    }
    /*createDrinks() {

    }*/
}

/***/ }),

/***/ "./src/Products.js":
/*!*************************!*\
  !*** ./src/Products.js ***!
  \*************************/
/*! exports provided: Products */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Products", function() { return Products; });
class Products {
    /**
     *
     * @param {string} name 
     * @param {integer} id
     */
    constructor(name, id, source) {
        this.name = name;
        this.id = id;
        this.source = source;
    }
    drawProducts() {
        /*let snackButton = document.createElement('div');
        snackButton.classList.add('snackButton');*/
        let snack = document.createElement('img');
        snack.classList.add('img');
        // 1. Pour afficher des images différentes, il faut mettre la source en paramètre dans le constructeur de Products et utiliser this pour pointer dessus
        snack.src = this.source;
        return snack;
    }

    showProducts() {
        alert(`${this.name} costs ${this.price} and its id is: ${this.id}.`);

    }
    choseProducts() {
            alert(`You chose ${this.name} you have to pay ${this.price}`);
        }
        /*dropProducts() {

        }*/
}

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: allProducts */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "allProducts", function() { return allProducts; });
/* harmony import */ var _Drinks__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Drinks */ "./src/Drinks.js");
/* harmony import */ var _Candies__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Candies */ "./src/Candies.js");
/* harmony import */ var _Products__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Products */ "./src/Products.js");




// 3. Penser à ajouter les url sources pour chaque produit dans sa construction
const allProducts = [
    new _Drinks__WEBPACK_IMPORTED_MODULE_0__["Drinks"]("ginger beer", 1, "./img/gingerbeer.jpg", 7, ),
    new _Drinks__WEBPACK_IMPORTED_MODULE_0__["Drinks"]("vodka lemon", 2, "./img/vodkalemon.jpg", 10),
    new _Drinks__WEBPACK_IMPORTED_MODULE_0__["Drinks"]("charitea", 3, "./img/charitea.jpg", 5),
    new _Drinks__WEBPACK_IMPORTED_MODULE_0__["Drinks"]("chouffe", 4, "./img/chouffe.png", 7),
    new _Drinks__WEBPACK_IMPORTED_MODULE_0__["Drinks"]("ice americano", 5, "./img/iceamericano.jpg", 4),
    new _Drinks__WEBPACK_IMPORTED_MODULE_0__["Drinks"]("green tea", 6, "./img/greentea.jpg", 5),
    new _Drinks__WEBPACK_IMPORTED_MODULE_0__["Drinks"]("trappist", 7, "./img/trappiste.png", 7),
    new _Drinks__WEBPACK_IMPORTED_MODULE_0__["Drinks"]("cuvee des trolls", 8, "./img/cuveedestrolls.jpg", 8),
    new _Drinks__WEBPACK_IMPORTED_MODULE_0__["Drinks"]("milk", 9, "./img/milk.jpg", 3),
    new _Candies__WEBPACK_IMPORTED_MODULE_1__["Candies"]("bubblies", 10, "./img/bubblizz.png", 3),
    new _Candies__WEBPACK_IMPORTED_MODULE_1__["Candies"]("eggs", 11, "./img/eggs.jpg", 5),
    new _Candies__WEBPACK_IMPORTED_MODULE_1__["Candies"]("langue de chat", 12, "./img/languedechat.png", 6),
    new _Candies__WEBPACK_IMPORTED_MODULE_1__["Candies"]("Arlequin", 13, "./img/arlequin.png", 10000),
    new _Candies__WEBPACK_IMPORTED_MODULE_1__["Candies"]("Dragibus", 14, "./img/dragibus.jpg", 7),
    new _Candies__WEBPACK_IMPORTED_MODULE_1__["Candies"]("Marshmallow", 15, "./img/chamallow.png", 3)
];
let container = document.querySelector('#container');

for (const product of allProducts) {
    // On applique la méthode draw pour chaque produit
    // et on l'appendchild dans le container
    // On le stocke dans une variable pour pouvoir mettre 
    // un eventlistener dessus
    let item = container.appendChild(product.drawProducts());
    item.addEventListener('click', function() {
        product.showProducts();
    });
};

// au click sur le boutton submit, on lance la méthode choseProducts sur l'index du produit concerné
let submitBtn = document.querySelector('#submit');
let choiceInput = document.querySelector('#choice');
submitBtn.addEventListener('click', function() {
    let choice = choiceInput.value;
    allProducts[parseInt(choice) - 1].choseProducts();
});

// Et maintenant faut afficher l'image voulue dans le réceptacle en bas ;)

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL0NhbmRpZXMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL0RyaW5rcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvUHJvZHVjdHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFBQTtBQUFBO0FBQXNDOztBQUUvQixzQkFBc0Isa0RBQVE7QUFDckM7QUFDQTtBQUNBLGVBQWUsUUFBUTtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsS0FBSztBQUNMLEM7Ozs7Ozs7Ozs7OztBQ2RBO0FBQUE7QUFBQTtBQUFzQzs7QUFFL0IscUJBQXFCLGtEQUFRO0FBQ3BDO0FBQ0E7QUFDQSxlQUFlLFFBQVE7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLEtBQUs7QUFDTCxDOzs7Ozs7Ozs7Ozs7QUNkQTtBQUFBO0FBQU87QUFDUDtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsUUFBUTtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQWlEO0FBQ2pEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGlCQUFpQixVQUFVLFNBQVMsV0FBVyxrQkFBa0IsUUFBUTs7QUFFekU7QUFDQTtBQUNBLCtCQUErQixVQUFVLG1CQUFtQixXQUFXO0FBQ3ZFO0FBQ0E7O0FBRUEsU0FBUztBQUNULEM7Ozs7Ozs7Ozs7OztBQy9CQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWtDO0FBQ0U7QUFDRTs7QUFFdEM7QUFDTztBQUNQLFFBQVEsOENBQU07QUFDZCxRQUFRLDhDQUFNO0FBQ2QsUUFBUSw4Q0FBTTtBQUNkLFFBQVEsOENBQU07QUFDZCxRQUFRLDhDQUFNO0FBQ2QsUUFBUSw4Q0FBTTtBQUNkLFFBQVEsOENBQU07QUFDZCxRQUFRLDhDQUFNO0FBQ2QsUUFBUSw4Q0FBTTtBQUNkLFFBQVEsZ0RBQU87QUFDZixRQUFRLGdEQUFPO0FBQ2YsUUFBUSxnREFBTztBQUNmLFFBQVEsZ0RBQU87QUFDZixRQUFRLGdEQUFPO0FBQ2YsUUFBUSxnREFBTztBQUNmO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHlFQUF5RSxDIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsImltcG9ydCB7IFByb2R1Y3RzIH0gZnJvbSBcIi4vUHJvZHVjdHNcIjtcblxuZXhwb3J0IGNsYXNzIENhbmRpZXMgZXh0ZW5kcyBQcm9kdWN0cyB7XG4gICAgLyoqXG4gICAgICogXG4gICAgICogQHBhcmFtIHtpbnRlZ2VyfSBwcmljZSBcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcihuYW1lLCBpZCwgc291cmNlLCBwcmljZSkge1xuICAgICAgICBzdXBlcihuYW1lLCBpZCxzb3VyY2UpO1xuICAgICAgICB0aGlzLnByaWNlID0gcHJpY2U7XG4gICAgfVxuICAgIC8qY3JlYXRlQ2FuZHkoKSB7XG5cbiAgICB9Ki9cbn0iLCJpbXBvcnQgeyBQcm9kdWN0cyB9IGZyb20gXCIuL1Byb2R1Y3RzXCI7XG5cbmV4cG9ydCBjbGFzcyBEcmlua3MgZXh0ZW5kcyBQcm9kdWN0cyB7XG4gICAgLyoqXG4gICAgICogXG4gICAgICogQHBhcmFtIHtpbnRlZ2VyfSBwcmljZSBcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcihuYW1lLCBpZCwgc291cmNlLCBwcmljZSkge1xuICAgICAgICBzdXBlcihuYW1lLCBpZCwgc291cmNlKTtcbiAgICAgICAgdGhpcy5wcmljZSA9IHByaWNlO1xuICAgIH1cbiAgICAvKmNyZWF0ZURyaW5rcygpIHtcblxuICAgIH0qL1xufSIsImV4cG9ydCBjbGFzcyBQcm9kdWN0cyB7XG4gICAgLyoqXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gbmFtZSBcbiAgICAgKiBAcGFyYW0ge2ludGVnZXJ9IGlkXG4gICAgICovXG4gICAgY29uc3RydWN0b3IobmFtZSwgaWQsIHNvdXJjZSkge1xuICAgICAgICB0aGlzLm5hbWUgPSBuYW1lO1xuICAgICAgICB0aGlzLmlkID0gaWQ7XG4gICAgICAgIHRoaXMuc291cmNlID0gc291cmNlO1xuICAgIH1cbiAgICBkcmF3UHJvZHVjdHMoKSB7XG4gICAgICAgIC8qbGV0IHNuYWNrQnV0dG9uID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgIHNuYWNrQnV0dG9uLmNsYXNzTGlzdC5hZGQoJ3NuYWNrQnV0dG9uJyk7Ki9cbiAgICAgICAgbGV0IHNuYWNrID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaW1nJyk7XG4gICAgICAgIHNuYWNrLmNsYXNzTGlzdC5hZGQoJ2ltZycpO1xuICAgICAgICAvLyAxLiBQb3VyIGFmZmljaGVyIGRlcyBpbWFnZXMgZGlmZsOpcmVudGVzLCBpbCBmYXV0IG1ldHRyZSBsYSBzb3VyY2UgZW4gcGFyYW3DqHRyZSBkYW5zIGxlIGNvbnN0cnVjdGV1ciBkZSBQcm9kdWN0cyBldCB1dGlsaXNlciB0aGlzIHBvdXIgcG9pbnRlciBkZXNzdXNcbiAgICAgICAgc25hY2suc3JjID0gdGhpcy5zb3VyY2U7XG4gICAgICAgIHJldHVybiBzbmFjaztcbiAgICB9XG5cbiAgICBzaG93UHJvZHVjdHMoKSB7XG4gICAgICAgIGFsZXJ0KGAke3RoaXMubmFtZX0gY29zdHMgJHt0aGlzLnByaWNlfSBhbmQgaXRzIGlkIGlzOiAke3RoaXMuaWR9LmApO1xuXG4gICAgfVxuICAgIGNob3NlUHJvZHVjdHMoKSB7XG4gICAgICAgICAgICBhbGVydChgWW91IGNob3NlICR7dGhpcy5uYW1lfSB5b3UgaGF2ZSB0byBwYXkgJHt0aGlzLnByaWNlfWApO1xuICAgICAgICB9XG4gICAgICAgIC8qZHJvcFByb2R1Y3RzKCkge1xuXG4gICAgICAgIH0qL1xufSIsImltcG9ydCB7IERyaW5rcyB9IGZyb20gXCIuL0RyaW5rc1wiO1xuaW1wb3J0IHsgQ2FuZGllcyB9IGZyb20gXCIuL0NhbmRpZXNcIjtcbmltcG9ydCB7IFByb2R1Y3RzIH0gZnJvbSBcIi4vUHJvZHVjdHNcIjtcblxuLy8gMy4gUGVuc2VyIMOgIGFqb3V0ZXIgbGVzIHVybCBzb3VyY2VzIHBvdXIgY2hhcXVlIHByb2R1aXQgZGFucyBzYSBjb25zdHJ1Y3Rpb25cbmV4cG9ydCBjb25zdCBhbGxQcm9kdWN0cyA9IFtcbiAgICBuZXcgRHJpbmtzKFwiZ2luZ2VyIGJlZXJcIiwgMSwgXCIuL2ltZy9naW5nZXJiZWVyLmpwZ1wiLCA3LCApLFxuICAgIG5ldyBEcmlua3MoXCJ2b2RrYSBsZW1vblwiLCAyLCBcIi4vaW1nL3ZvZGthbGVtb24uanBnXCIsIDEwKSxcbiAgICBuZXcgRHJpbmtzKFwiY2hhcml0ZWFcIiwgMywgXCIuL2ltZy9jaGFyaXRlYS5qcGdcIiwgNSksXG4gICAgbmV3IERyaW5rcyhcImNob3VmZmVcIiwgNCwgXCIuL2ltZy9jaG91ZmZlLnBuZ1wiLCA3KSxcbiAgICBuZXcgRHJpbmtzKFwiaWNlIGFtZXJpY2Fub1wiLCA1LCBcIi4vaW1nL2ljZWFtZXJpY2Fuby5qcGdcIiwgNCksXG4gICAgbmV3IERyaW5rcyhcImdyZWVuIHRlYVwiLCA2LCBcIi4vaW1nL2dyZWVudGVhLmpwZ1wiLCA1KSxcbiAgICBuZXcgRHJpbmtzKFwidHJhcHBpc3RcIiwgNywgXCIuL2ltZy90cmFwcGlzdGUucG5nXCIsIDcpLFxuICAgIG5ldyBEcmlua3MoXCJjdXZlZSBkZXMgdHJvbGxzXCIsIDgsIFwiLi9pbWcvY3V2ZWVkZXN0cm9sbHMuanBnXCIsIDgpLFxuICAgIG5ldyBEcmlua3MoXCJtaWxrXCIsIDksIFwiLi9pbWcvbWlsay5qcGdcIiwgMyksXG4gICAgbmV3IENhbmRpZXMoXCJidWJibGllc1wiLCAxMCwgXCIuL2ltZy9idWJibGl6ei5wbmdcIiwgMyksXG4gICAgbmV3IENhbmRpZXMoXCJlZ2dzXCIsIDExLCBcIi4vaW1nL2VnZ3MuanBnXCIsIDUpLFxuICAgIG5ldyBDYW5kaWVzKFwibGFuZ3VlIGRlIGNoYXRcIiwgMTIsIFwiLi9pbWcvbGFuZ3VlZGVjaGF0LnBuZ1wiLCA2KSxcbiAgICBuZXcgQ2FuZGllcyhcIkFybGVxdWluXCIsIDEzLCBcIi4vaW1nL2FybGVxdWluLnBuZ1wiLCAxMDAwMCksXG4gICAgbmV3IENhbmRpZXMoXCJEcmFnaWJ1c1wiLCAxNCwgXCIuL2ltZy9kcmFnaWJ1cy5qcGdcIiwgNyksXG4gICAgbmV3IENhbmRpZXMoXCJNYXJzaG1hbGxvd1wiLCAxNSwgXCIuL2ltZy9jaGFtYWxsb3cucG5nXCIsIDMpXG5dO1xubGV0IGNvbnRhaW5lciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNjb250YWluZXInKTtcblxuZm9yIChjb25zdCBwcm9kdWN0IG9mIGFsbFByb2R1Y3RzKSB7XG4gICAgLy8gT24gYXBwbGlxdWUgbGEgbcOpdGhvZGUgZHJhdyBwb3VyIGNoYXF1ZSBwcm9kdWl0XG4gICAgLy8gZXQgb24gbCdhcHBlbmRjaGlsZCBkYW5zIGxlIGNvbnRhaW5lclxuICAgIC8vIE9uIGxlIHN0b2NrZSBkYW5zIHVuZSB2YXJpYWJsZSBwb3VyIHBvdXZvaXIgbWV0dHJlIFxuICAgIC8vIHVuIGV2ZW50bGlzdGVuZXIgZGVzc3VzXG4gICAgbGV0IGl0ZW0gPSBjb250YWluZXIuYXBwZW5kQ2hpbGQocHJvZHVjdC5kcmF3UHJvZHVjdHMoKSk7XG4gICAgaXRlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAgICAgICBwcm9kdWN0LnNob3dQcm9kdWN0cygpO1xuICAgIH0pO1xufTtcblxuLy8gYXUgY2xpY2sgc3VyIGxlIGJvdXR0b24gc3VibWl0LCBvbiBsYW5jZSBsYSBtw6l0aG9kZSBjaG9zZVByb2R1Y3RzIHN1ciBsJ2luZGV4IGR1IHByb2R1aXQgY29uY2VybsOpXG5sZXQgc3VibWl0QnRuID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3N1Ym1pdCcpO1xubGV0IGNob2ljZUlucHV0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2Nob2ljZScpO1xuc3VibWl0QnRuLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24oKSB7XG4gICAgbGV0IGNob2ljZSA9IGNob2ljZUlucHV0LnZhbHVlO1xuICAgIGFsbFByb2R1Y3RzW3BhcnNlSW50KGNob2ljZSkgLSAxXS5jaG9zZVByb2R1Y3RzKCk7XG59KTtcblxuLy8gRXQgbWFpbnRlbmFudCBmYXV0IGFmZmljaGVyIGwnaW1hZ2Ugdm91bHVlIGRhbnMgbGUgcsOpY2VwdGFjbGUgZW4gYmFzIDspIl0sInNvdXJjZVJvb3QiOiIifQ==